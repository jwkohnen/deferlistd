# WIP

This software is in its very early stages and doesn't work for any
purpose.

# mxdefer: a small mail exchanger's spam deferrer

mxdeferd and mxdefer are a daemon and its CLI client that can be
installed with mail exchangers (mx), also called mail transport
agents (MTA).  mxdefer tells the mx to defer, i.e. temporarily
reject, incoming email unless the sender has been seen before.
The idea being that by forcing the sendind mx to retry sending
the email the mx "proves" that it is behaves like a legit sending
server.  A lot of spam sending agents do not pass this test.

Back in the day this method was very, very effective.  The world has
moved on, but it is my believe that this spam deterrent still has a
place at small installations that are operated by low volume domains
like personal mail domains.

This concept is not new and is popular under the term "greylisting."
I subscribe to the idea that language that happens to associate 
with human trafficing history also happens to not be descriptive.
I let you decide which of the two reasons is the better one to
motivate using more precise terms.

A mail exchanger responds to incoming mail transport protocol
requests with one of three responses: allow (2xx codes), defer
(4xx codes) and deny (5xx codes). This is excactly what mxdefer
helps the MX to do, defer being the special case that is at
mxdefer's core.

## Deferring incoming mail like it's 2024

WIP

## Compatible MX software

This implementation is developed with for use with Exim. 
